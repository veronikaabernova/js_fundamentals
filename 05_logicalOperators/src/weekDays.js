let value = (prompt("Enter your number : "));
let number = Number(value);
let message;

if (Number.isNaN(number) || !Number.isInteger(number)){
    message = "incorrect format";
} else {
    message = "Day : ";
    switch(number){
        case 1:{
            message += "Monday";
            break;
        }
        case 2:{
            message += "Tuesday";
            break;
        }
        case 3:{
            message += "Wednesday";
            break;
        }
        case 4:{
            message += "Thursday";
            break;
        }
        case 5:{
            message += "Friday";
            break;
        }
        case 6:{
            message += "Saturday";
            break;
        }
        case 7:{
            message += "Sunday";
        }
        default:{
            message = "Enter number from 1 to 7";
        }
    } 

}
console.log(message);